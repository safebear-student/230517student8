package com.safebear.app;

import com.safebear.app.pages.WelcomePage;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class Test01_Login extends BaseTest{

    @Test
    public void testLogin() {
        //step 1 Confirm we're on the Welcome Page
        assertTrue(this.welcomePage.checkCorrectPage());
        //step 2 click on the Login link and the Login Page loads
        assertTrue(this.welcomePage.clickOnLogin(this.loginPage));
        //step 3 Login with valid credentials
        assertTrue(loginPage.login(this.userPage, "testuser", "testing"));

    }
}
