package com.safebear.app.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class UserPage {

    WebDriver driver;

    public UserPage(WebDriver driver)
    {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public boolean checkCorrectPage() {
        return driver.getTitle().startsWith("Logged In");
    }
}
