package com.safebear.app.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class WelcomePage {

    @FindBy(linkText = "Login")
    WebElement loginLink1;

    @FindBy(xpath = "//*[@id=\"navbar\"]/ul/li[2]/a")
    WebElement loginLink2;

    WebDriver driver;

    public WelcomePage(WebDriver driver)
    {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public Boolean clickOnLogin(LoginPage loginPage)
    {
        loginLink1.click();
        return loginPage.checkCorrectPage();
    }

    public Boolean checkCorrectPage() {
        return driver.getTitle().startsWith("Welcome");
    }

}
